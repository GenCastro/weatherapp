//
//  DayForecastCell.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/29.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

class DayForecastCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(day: String, icon: String, temp: String) {
        
        dayLabel.text = day
        iconView.setIcon(code: icon)
        tempLabel.text = temp
    }
}
