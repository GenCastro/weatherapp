//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    @IBOutlet weak var mainTempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var location: CLLocation!
    var viewModel: WeatherViewModel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard location != nil else {
            fatalError("No location")
        }

        viewModel = WeatherViewModel()
        fetchWeather()
    }
    
    func fetchWeather() {
        let lon = location.coordinate.longitude.description
        let lat = location.coordinate.latitude.description
        getCurrentWeather(lon: lon, lat: lat)
        fetchFiveDayForecast(lon: lon, lat: lat)
    }
    
    func getCurrentWeather(lon: String, lat: String) {
        viewModel.fetchCurrentWeather(lon: lon, lat: lat) { weather, error in
            guard let weather = weather else {
                return self.showError(error: error ?? AppError.unknownError)
            }
            
            self.setupView(weather: weather)
        }
    }
    
    func fetchFiveDayForecast(lon: String, lat: String) {
        viewModel.fetchFiveDayForecast(lon: lon, lat: lat) { weather, error in
            guard let error = error else {
                return self.tableView.reloadData()
            }
            
            self.showError(error: error)
        }
    }
    
    func setupView(weather: WeatherInfo) {
        mainTempLabel.text = weather.temp
        descriptionLabel.text = weather.description
        currentTempLabel.text = weather.temp
        minTempLabel.text = weather.minTemp
        maxTempLabel.text = weather.maxTemp
        contentView.isHidden = false
        contentView.backgroundColor = viewModel.theme.Background.color
        imgView.image = viewModel.theme.Background.image
        tableView.backgroundColor = viewModel.theme.Background.color
    }
    
    func showError(error: AppError) {
        let alert = UIAlertAction(title: Strings.Alert.retry, style: .default, handler: { _ in
            self.fetchWeather()
        })
        showAlert(msg: error.description, actions: [alert])
    }
}

extension WeatherViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.daysForecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DayForecastCell", for: indexPath) as? DayForecastCell else {
            fatalError("could not load DayForecastCell")
        }
        
        cell.contentView.backgroundColor = viewModel.theme.Background.color
        let weather = viewModel.daysForecast[indexPath.row]
        cell.configure(day: weather.date.string(format: "EEEE"), icon: weather.icon, temp: weather.temp)
        return cell
    }
}
