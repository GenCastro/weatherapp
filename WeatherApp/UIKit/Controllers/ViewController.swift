//
//  ViewController.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    var location: CLLocationManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        location = CLLocationManager()
        location.delegate = self;
        location.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        switch CLLocationManager.authorizationStatus() {
            
        case .authorizedAlways, .authorizedWhenInUse:
            location.startUpdatingLocation()
        default :
            location.stopUpdatingLocation()
            location.requestAlwaysAuthorization()
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            let iKnowAction = UIAlertAction(title: "I know", style: .cancel, handler: nil)
            let allowAcessAction = UIAlertAction(title: "allow", style: .default) { (_) in
                guard let url = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            showAlert(msg: Strings.Location.accessDenied, actions: [iKnowAction, allowAcessAction])
        case .authorizedAlways, .authorizedWhenInUse:
               location.startUpdatingLocation()
        default:
                location.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location.stopUpdatingLocation()
        print(locations[0].coordinate)
        
        guard let weatherController = storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController else {
            return
        }
        weatherController.location = locations[0]
        present(weatherController, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showAlert(msg: error.localizedDescription)
    }
}
