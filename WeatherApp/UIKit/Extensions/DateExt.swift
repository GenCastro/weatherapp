//
//  DateExt.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/28.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

extension Date {
    func string(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
