//
//  ImageViewExt.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/14.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {

    func setIcon(code: String) {
        guard let url = URL(string: "https://openweathermap.org/img/w/\(code).png") else {
            return
        }
        
        af_setImage(withURL: url)
    }
}
