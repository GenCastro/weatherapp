//
//  UIViewControllerExt.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/14.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String = "", msg: String, actions: [UIAlertAction]? = nil, onCompletion: BasicHandler? = nil ) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        if let actions = actions {
            actions.forEach { (action) in
                alert.addAction(action)
            }
        } else {
            alert.addAction(UIAlertAction(title: Strings.Alert.defaultOkText, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
