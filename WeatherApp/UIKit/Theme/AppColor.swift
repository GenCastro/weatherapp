//
//  AppColor.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/28.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

struct AppColor {
    static let sunny = UIColor(hex: 0x47AB2F)
    static let cloudy = UIColor(hex: 0x54717A)
    static let rainy = UIColor(hex: 0x57575D)
}
