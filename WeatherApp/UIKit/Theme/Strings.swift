//
//  Strings.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/14.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct Strings {
    
    struct Alert {
        static let retry = "Retry"
        static let defaultOkText = "Ok"
    }
    
    struct Location {
        static let accessDenied = "You have denied us access to your location, unfortunately we can not help you if you do not trust us :)"
    }
    
    struct Error {
        static let decodeFail = "Oops, we have done something terribly wrong...excuse us"
        static let unknownError = "An unexpected error has occurred. please try again."
    }
}
