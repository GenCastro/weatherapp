//
//  AppTheme.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/29.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

typealias Background = (image: UIImage, color: UIColor)

enum AppTheme {
    case sunny,rainy,cloudy
    
    var Background: Background {
        switch self {
        case .sunny:
            return (AppImage.sunny, AppColor.sunny)
        case .cloudy:
            return (AppImage.cloudy, AppColor.cloudy)
        case .rainy:
            return (AppImage.rainy, AppColor.rainy)
        }
    }
}
