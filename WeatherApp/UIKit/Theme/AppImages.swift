//
//  AppImages.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/28.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

struct AppImage {
    static let sunny = #imageLiteral(resourceName: "forest_sunny")
    static let cloudy = #imageLiteral(resourceName: "forest_cloudy")
    static let rainy = #imageLiteral(resourceName: "forest_rainy")
}
