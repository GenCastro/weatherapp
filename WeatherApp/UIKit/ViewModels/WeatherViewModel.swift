//
//  CurrentWeatherViewModel.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import UIKit

class WeatherViewModel {

    var theme: AppTheme {
        guard let iconNumber = Int(currentWeather.icon.dropLast()) else {
            return .cloudy
        }
        
        switch iconNumber {
        case 1,2:
            return .sunny
        case 3,4:
            fallthrough
        case 9...13:
            return .rainy
        default:
            return .cloudy
        }
    }
    
    var currentWeather: WeatherInfo!
    var daysForecast: [WeatherInfo] = []
    
    init() {
        
    }
    
    func fetchCurrentWeather(lon: String, lat: String, handler: @escaping CurrentWeatherResponseHandler) {
        WeatherServices().getCurrentWeather(request: CurrentWeatherRequest(lon: lon, lat: lat)) { weather, error in
            self.currentWeather = weather
            handler(weather,error)
        }
    }
    
    func fetchFiveDayForecast(lon: String, lat: String, handler: @escaping FiveDayForecastResponseHandler) {
        WeatherServices().getFiveDayForecast(request: FiveDayForecastRequest(lon: lon, lat: lat)) { weather, error in
            
            guard let weatherList = weather else {
                return handler(nil, error)
            }
            
            var midDayWeather = weatherList.filter({ (weather) -> Bool in
                let strDate = weather.date.string(format: "HH:mm")
                return strDate == "12:00"
            })
            
            let calendar = Calendar.current
            if calendar.isDateInToday(midDayWeather[0].date) {
                _ = midDayWeather.remove(at: 0)
            }
            
            self.daysForecast = midDayWeather
            handler(midDayWeather, nil)
        }
    }
}
