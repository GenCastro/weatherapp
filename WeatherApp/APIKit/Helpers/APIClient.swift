//
//  APIClient.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

class ApiClient {
    
    typealias ResponseHandler = (APIResponse) -> Void
    
    private func createURLRequest(request: HttpRequest) -> URLRequest {
        
        guard var components = URLComponents(url: request.url, resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components from \(request.url)")
        }
        
        let params = request.parameters?.merging(["APPID": "13ca73f238ac2584b011757f2bb80a76"], uniquingKeysWith: {(x,_) in x})
        components.queryItems = params?.map({ (key, value) -> URLQueryItem in
            URLQueryItem(name: key, value: value)
        })
        
        guard let finalURL = components.url else {
            fatalError("Unable to retrieve final URL")
        }
        
        print(finalURL)
        return URLRequest(url: finalURL)
    }
    
    func performRequest(request: HttpRequest, responseHandler: @escaping ResponseHandler) {
        
        let request = createURLRequest(request: request)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            if let error = error {
                responseHandler(.failure(error: AppError.apiError(message: error.localizedDescription)))
            } else {
                guard let data = data else {
                    return responseHandler(.failure(error: .unknownError))
                }

                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }
                
                responseHandler(.success(result: data))
            }
        })
        task.resume()
    }
}

func load(filename fileName: String) -> Data? {
    let sdkModule = Bundle(for: ApiClient.self)
    guard let url = sdkModule.url(forResource: fileName, withExtension: "json") else  {
        return nil
    }
    
    print("file found")
    
    do {
        let data = try Data(contentsOf: url)
        return data
    } catch {
        return nil
    }
}
