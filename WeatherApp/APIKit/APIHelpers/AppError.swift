//
//  AppError.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

enum AppError: Error, LocalizedError {
    case apiError(message: String)
    case unknownError
    case decodeFail
    
    var description: String {
        switch self {
        case .apiError(let msg):
            return msg
        case .decodeFail:
            return Strings.Error.decodeFail
        case .unknownError:
            return Strings.Error.unknownError
        }
    }
}
