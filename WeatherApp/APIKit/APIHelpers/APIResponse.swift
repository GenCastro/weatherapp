//
//  APIResponse.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

enum APIResponse {
    case success(result: Data)
    case failure(error: AppError)
}
