//
//  HttpRequest.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

protocol HttpRequest {
    var url: URL {get}
    var parameters: [String: String]? {get set}
}
