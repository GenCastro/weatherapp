//
//  Endpoints.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct Endoints {
    private static let baseUrl = "https://api.openweathermap.org/"
    
    struct Weather {
        private static let route = "data/2.5/"
        static let current = "\(baseUrl)\(route)weather"
        static let forecast5 = "\(baseUrl)\(route)forecast"
    }
}
