//
//  Globals.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

typealias CurrentWeatherResponseHandler = (_ weather: WeatherInfo?, _ error: AppError?) -> Void
typealias FiveDayForecastResponseHandler = (_ weather: [WeatherInfo]?, _ error: AppError?) -> Void
typealias BasicHandler = () -> Void
