//
//  FiveDayForecastResponse.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/28.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct FiveDayForecastResponse: Codable {
    private var list: [WeatherInfo]
    
    var fiveDayforecast: [WeatherInfo] {
        return list
    }
}
