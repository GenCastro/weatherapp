//
//  CurrentWeatherRequest.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct CurrentWeatherRequest: HttpRequest {
    
    var url: URL {
        guard let url = URL(string: Endoints.Weather.current) else {
            fatalError("Couldn't create url from: \(Endoints.Weather.current)")
        }
        
        return url
    }
    
    var parameters: [String : String]?
    
    init(lon: String, lat: String) {
        parameters = ["lon": lon, "lat": lat, "units": "metric"]
    }
}
