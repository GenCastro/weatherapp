//
//  FiveDayForecastRequest.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/27.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct FiveDayForecastRequest: HttpRequest {
    
    var url: URL {
        guard let url = URL(string: Endoints.Weather.forecast5) else {
            fatalError("Couldn't create url from: \(Endoints.Weather.forecast5)")
        }
        
        return url
    }
    
    var parameters: [String : String]?
    
    init(lon: String, lat: String) {
        parameters = ["lon": lon, "lat": lat, "units": "metric"]
    }
}
