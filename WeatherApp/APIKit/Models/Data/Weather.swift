//
//  Weather.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

struct WeatherInfo: Codable {
    
    private struct Main: Codable {
        var temp: Double
        var temp_min: Double
        var temp_max: Double
    }
    
    private struct Weather: Codable {
        var icon: String
        var description: String
    }
    
    private var main: Main
    private var weather: [Weather]
    private var dt: TimeInterval
    
    var description: String {
        return weather.first?.description ?? ""
    }
    var icon: String {
        return weather.first?.icon ?? ""
    }
    var temp: String {
        return String(format: "%.0f%@", arguments: [main.temp, "\u{00B0}"])
    }
    var minTemp: String {
        return String(format: "%.0f%@", arguments: [main.temp_min, "\u{00B0}"])
    }
    var maxTemp: String {
        return String(format: "%.0f%@", arguments: [main.temp_max, "\u{00B0}"])
    }
    var date: Date {
        let date = Date(timeIntervalSince1970: dt)
        return date
    }
}
