//
//  WeatherServices.swift
//  WeatherApp
//
//  Created by zwelithini.mathebula on 2018/06/13.
//  Copyright © 2018 zwelithini.mathebula. All rights reserved.
//

import Foundation

class WeatherServices {
    
    func getCurrentWeather(request: CurrentWeatherRequest, completionHandler: @escaping CurrentWeatherResponseHandler) {
        ApiClient().performRequest(request: request) { (response) in
        
            switch response {
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }
            case .success(let result):

                do {
                    let weather = try JSONDecoder().decode(WeatherInfo.self, from: result)
                    DispatchQueue.main.async {
                        completionHandler(weather, nil)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completionHandler(nil, AppError.decodeFail)
                    }
                }
            }
        }
    }
    
    func getFiveDayForecast(request: FiveDayForecastRequest, completionHandler: @escaping FiveDayForecastResponseHandler) {
        ApiClient().performRequest(request: request) { (response) in

            switch response {
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }
            case .success(let result):

                do {
                    let list = try JSONDecoder().decode(FiveDayForecastResponse.self, from: result)
                    DispatchQueue.main.async {
                        completionHandler(list.fiveDayforecast, nil)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completionHandler(nil, AppError.decodeFail)
                    }
                }
            }
        }
    }
    
}
